import subprocess
import shlex
import os

from src.config import config

PATH_TO_BERT = config['preprocessing']['PATH_TO_BERT']
TASK = config['validate']['TASK']
MODE = config['validate']['MODE']
TEST_ALL = config['validate']['TEST_ALL']
BATCH_SIZE = config['validate']['BATCH_SIZE']
TEST_BATCH_SIZE = config['validate']['TEST_BATCH_SIZE']
BERT_DATA_PATH = config['validate']['BERT_DATA_PATH']
LOG_FILE = config['validate']['LOG_FILE']
MODEL_PATH = config['validate']['MODEL_PATH']
SEP_OPTIM = config['validate']['SEP_OPTIM']
USE_INTERVAL = config['validate']['USE_INTERVAL']
VISIBLE_GPUS = config['validate']['VISIBLE_GPUS']
MAX_POS = config['validate']['MAX_POS']
MAX_LENGTH = config['validate']['MAX_LENGTH']
ALPHA = config['validate']['ALPHA']
MIN_LENGTH = config['validate']['MIN_LENGTH']
RESULT_PATH = config['validate']['RESULT_PATH']

os.environ ['BERT_PATH'] = PATH_TO_BERT


if __name__ == '__main__':
    subprocess.call(
        shlex.split(
            f'python ./src/PreSumm/src/train.py\
            -task {TASK}\
            -mode {MODE}\
            -test_all {TEST_ALL}\
            -batch_size {BATCH_SIZE}\
            -test_batch_size {TEST_BATCH_SIZE}\
            -bert_data_path {BERT_DATA_PATH}\
            -log_file {LOG_FILE}\
            -model_path {MODEL_PATH}\
            -sep_optim {SEP_OPTIM}\
            -use_interval {USE_INTERVAL}\
            -visible_gpus {VISIBLE_GPUS}\
            -max_pos {MAX_POS}\
            -max_length {MAX_LENGTH}\
            -alpha {ALPHA}\
            -min_length {MIN_LENGTH}\
            -result_path {RESULT_PATH}'
            )
            )
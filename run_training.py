import subprocess
import shlex
import os

from src.config import config

PATH_TO_BERT = config['preprocessing']['PATH_TO_BERT']
TASK = config['train']['TASK']
MODE = config['train']['MODE']
BERT_DATA_PATH = config['train']['BERT_DATA_PATH']
DEC_DROPOUT = config['train']['DEC_DROPOUT']
MODEL_PATH = config['train']['MODEL_PATH']

SEP_OPTIM = config['train']['SEP_OPTIM']
LR_BERT = config['train']['LR_BERT']
LR_DEC = config['train']['LR_DEC']
DEC_LAYERS = config['train']['DEC_LAYERS']
SAVE_CHECKPOINT_STEPS = config['train']['SAVE_CHECKPOINT_STEPS']

BATCH_SIZE = config['train']['BATCH_SIZE']
TRAIN_STEPS = config['train']['TRAIN_STEPS']
REPORT_EVERY = config['train']['REPORT_EVERY']
ACCUM_COUNT = config['train']['ACCUM_COUNT']
USE_BERT_EMB = config['train']['USE_BERT_EMB']

USE_INTERVAL = config['train']['USE_INTERVAL']
WARMUP_STEPS_BERT = config['train']['WARMUP_STEPS_BERT']
WARMUP_STEPS_DEC = config['train']['WARMUP_STEPS_DEC']
MAX_POS = config['train']['MAX_POS']
VISIBLE_GPUS = config['train']['VISIBLE_GPUS']
LOG_FILE = config['train']['LOG_FILE']

os.environ ['BERT_PATH'] = PATH_TO_BERT


if __name__ == '__main__':
    subprocess.call(
        shlex.split(
            f'python ./src/PreSumm/src/train.py\
            -task {TASK}\
            -mode {MODE}\
            -bert_data_path {BERT_DATA_PATH}\
            -dec_dropout {DEC_DROPOUT}\
            -model_path {MODEL_PATH}\
            -sep_optim {SEP_OPTIM}\
            -lr_bert {LR_BERT}\
            -lr_dec {LR_DEC}\
            -dec_layers {DEC_LAYERS}\
            -save_checkpoint_steps {SAVE_CHECKPOINT_STEPS}\
            -batch_size {BATCH_SIZE}\
            -train_steps {TRAIN_STEPS}\
            -report_every {REPORT_EVERY}\
            -accum_count {ACCUM_COUNT}\
            -use_bert_emb {USE_BERT_EMB}\
            -use_interval {USE_INTERVAL}\
            -warmup_steps_bert {WARMUP_STEPS_BERT}\
            -warmup_steps_dec {WARMUP_STEPS_DEC}\
            -max_pos {MAX_POS}\
            -visible_gpus {VISIBLE_GPUS}\
            -log_file {LOG_FILE}'
            )
    )
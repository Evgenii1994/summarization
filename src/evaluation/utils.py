class Utils:

    @staticmethod
    def pretty_print_rouge(scores):
        print("rouge-1-f\trouge-2-f\t\trouge-l-f")
        print("\t".join([str(scores["rouge-1"]["f"])[:10],
                        str(scores["rouge-2"]["f"])[:10],
                        str(scores["rouge-l"]["f"])[:10]]))
        print("R-mean: ", (scores["rouge-1"]["f"] + scores["rouge-2"]["f"] + scores["rouge-l"]["f"]) / 3, '\n')
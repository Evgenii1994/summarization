import re
import logging

from rouge import Rouge
from nltk.tokenize import word_tokenize

from src.evaluation.utils import Utils

from src.config import config

logging.getLogger().setLevel(logging.INFO)

PATH_TEST_GOLD = config['evaluate']['PATH_TEST_GOLD']
PATH_TEST_PREDICT = config['evaluate']['PATH_TEST_PREDICT']
PATH_TEST_RAW_SRC = config['evaluate']['PATH_TEST_RAW_SRC']


class Evaluation:

    def __init__(self):
        with open(PATH_TEST_GOLD) as f:
            self.tgt_gold = f.read().splitlines()
        
        with open(PATH_TEST_PREDICT) as f:
            self.tgt_candidates = f.read().splitlines()

        with open(PATH_TEST_RAW_SRC) as f:
            self.raw_src = f.read().splitlines()

        self.rouge = Rouge()
        self.utils = Utils()

    def postprocess(self, text):
        text = text.replace("` ` ", '"').replace(" ' '", '"').replace("''", '"')\
            .replace('. . .',  '...').replace('<q>', '')

        text = text.replace(" ( ", " (").replace(" ) ", ") ")
        text = re.sub(r' ([.,:;?!%]+)([ \'"`])', r"\1\2", text)
        text = re.sub(r' ([.,:;?!%]+)$', r"\1", text)
        text = text.replace(" '", "'")
        text = text.replace(" ` ", " '")
        return text.strip()

    def evaluate(self):
        tgt_candidates = [self.postprocess(text) for text in self.tgt_candidates]
        tgt_gold = [self.postprocess(text) for text in self.tgt_gold]

        logging.info("Rouge scores postprocessed and detokenized")
        scores = self.rouge.get_scores(tgt_candidates, tgt_gold, avg=True)
        self.utils.pretty_print_rouge(scores)

        tgt_candidates = [" ".join(word_tokenize(text)) for text in tgt_candidates]
        tgt_gold = [" ".join(word_tokenize(text)) for text in tgt_gold]

        logging.info("Rouge scores postprocessed and tokenized")
        scores = self.rouge.get_scores(tgt_candidates, tgt_gold, avg=True)
        self.utils.pretty_print_rouge(scores)



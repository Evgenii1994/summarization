import json
import logging
import numpy as np
import pandas as pd

from summa import summarizer

from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize

from src.preprocessing.pytorch_data_preparer import PytorchDataPreparer
from src.preprocessing.data_cleaner import DataCleaner
from src.preprocessing.utils import Utils

from src.config import config

logging.getLogger().setLevel(logging.INFO)

from src.preprocessing.constants import (
    TOKENIZER,
    SAVE_PATH_VAL_PATTERN,
    SAVE_PATH_TEST_PATTERN,
    SAVE_PATH_TRAIN_PATTERN,
    SAVE_PATH_RAW_TEXT_PATTERN
)

SEED = config['preprocessing']['SEED']
PATH_TO_DATA = config['preprocessing']['PATH_TO_DATA']

TRAIN_SIZE = config['preprocessing']['TRAIN_SIZE']
VAL_SIZE = config['preprocessing']['VAL_SIZE']
TEST_SIZE = config['preprocessing']['TEST_SIZE']

GREEDY_ORACLE_CONST = config['preprocessing']['GREEDY_ORACLE_CONST']


class Preprocessing:

    def __init__(self):
        self.seed = SEED
        self.path = PATH_TO_DATA
        self.tokenizer = TOKENIZER

        self.data_cleaner = DataCleaner()
        self.pytorch_data_preparer = PytorchDataPreparer()
        self.utils = Utils()

    def _read_data(self):
        with open(self.path) as f:
            data = [json.loads(line) for line in f]
            data = pd.DataFrame(data)
            return data

    def _clean_data(self):
        data = self.data.transform([self.data_cleaner.cleaning_pipeline])
        data.columns = ['text', 'title']

        return data

    def _add_features(self):
        data = self.data

        data['text_len'] = data.text.transform(len)
        data['title_len'] = data.title.transform(len)
        
        data['text_n_words'] = data.text.str.split(r' ', expand=False).agg([len])
        data['title_n_words'] = data.title.str.split(r' ', expand=False).agg([len])
        
        data['text_n_sentences'] = data.text.transform([lambda text: len(self.tokenizer.tokenize(text))])
        data['title_n_sentences'] = data.title.transform([lambda title: len(self.tokenizer.tokenize(title))])

        data['title_is_fio'] = data.title.apply(lambda title: self.utils.check_fio(title))
        data['title_is_part_of_text'] = data.apply(lambda row: self.utils.check_fio_in_text(row), axis=1)

        return data

    def _filter_data(self):
        data = self.data

        data = data[
            (data.text_len <= 10000) &
            (data.title_n_sentences != 0) &
            (data.text_n_words > 1) &
            (data.text != data.title) &
            (data.text_len > data.title_len)
        ]

        data = data[
            ~((data.title_is_part_of_text == False) & (data.title_is_fio == True)) &
            ~((data.title_is_part_of_text == True) & (data.title_is_fio == True) & (data.title_n_words == 1))
        ].sort_values(by=['text_len', 'title_len'])

        return data

    def _split_data(self):
        data = self.data
        train_data, val_data, test_data = np.split(
            data.sample(frac=1, random_state=self.seed),
            [int(TRAIN_SIZE*len(data)), int((TRAIN_SIZE + VAL_SIZE)*len(data))]
        )
        return train_data, val_data, test_data

    def _prepare_tokenized_data(self, data):
        data_tokenized = [
            {
                "src": [word_tokenize(sent) for sent in sent_tokenize(text)],
                "tgt": [word_tokenize(sent) for sent in sent_tokenize(title)]
            } for text, title in data[['text', 'title']].values
        ]

        return data_tokenized

    def data_preparation_pipeline_raw_text(self, raw_texts):
        cleaned_texts = [self.data_cleaner.cleaning_pipeline(text) for text in raw_texts]

        titles = []
        for text in cleaned_texts:
            title_with_scores = sorted(
                summarizer.summarize(text, language='russian', scores=True, ratio=1.0),
                key = lambda x: x[1]
            )[-GREEDY_ORACLE_CONST:]
            title = [title for (title, score) in title_with_scores]
            titles.append(" ".join(title))

        data = pd.DataFrame(
            {'text': cleaned_texts, 'title': titles}
        )
        raw_text_tokenized = self._prepare_tokenized_data(data)
        self.pytorch_data_preparer.prepare_pytorch_data(
            raw_text_tokenized,
            SAVE_PATH_RAW_TEXT_PATTERN
            )

    def data_preparation_pipeline(self):
        logging.info("Starting to read data")
        self.data = self._read_data()
        logging.info("Finished reading data")

        logging.info("Starting to clean data")
        self.data = self._clean_data()
        logging.info("Finished reading data")

        logging.info("Starting to add features data")
        self.data = self._add_features()
        logging.info("Finished adding features data")

        logging.info("Starting to filter data")
        self.data = self._filter_data()
        logging.info("Finished filtering data")

        logging.info("Splitting data")
        train_data, val_data, test_data = self._split_data()

        logging.info("Starting to tokenize test data")
        test_data_tokenized = self._prepare_tokenized_data(test_data)
        logging.info("Finished tokenizing test data")

        logging.info("Starting to tokenize val data")
        val_data_tokenized = self._prepare_tokenized_data(val_data)
        logging.info("Finished tokenizing val data")

        logging.info("Starting to tokenize train data")
        train_data_tokenized = self._prepare_tokenized_data(train_data)
        logging.info("Finished tokenizeing train data")

        logging.info("Starting to prepare test pytorch data")
        self.pytorch_data_preparer.prepare_pytorch_data(
            test_data_tokenized,
            SAVE_PATH_TEST_PATTERN
            )
        logging.info("Finished preparing train pytorch data")

        logging.info("Starting to prepare val pytorch data")
        self.pytorch_data_preparer.prepare_pytorch_data(
            val_data_tokenized,
            SAVE_PATH_VAL_PATTERN
            )
        logging.info("Finished preparing val pytorch data")

        logging.info("Starting to prepare train pytorch data")
        self.pytorch_data_preparer.prepare_pytorch_data(
            train_data_tokenized,
            SAVE_PATH_TRAIN_PATTERN
            )
        logging.info("Finished preparing train pytorch data")

import copy
import torch

from tqdm import tqdm
from rouge import Rouge
from src.preprocessing.bert_data import BertData

from src.config import config

GREEDY_ORACLE_CONST = config['preprocessing']['GREEDY_ORACLE_CONST']
PATH_TO_BERT = config['preprocessing']['PATH_TO_BERT']
MAX_SENTENCES = config['preprocessing']['MAX_SENTENCES']
MAX_SRC_TOKENS = config['preprocessing']['MAX_SRC_TOKENS']
MAX_TGT_TOKENS = config['preprocessing']['MAX_TGT_TOKENS']
CHUNK_SIZE = config['preprocessing']['CHUNK_SIZE']
LOWER = config['preprocessing']['LOWER']


class PytorchDataPreparer:

    def __init__(self):
        self.bert = BertData(PATH_TO_BERT, LOWER, MAX_SRC_TOKENS, MAX_TGT_TOKENS)

    @staticmethod
    def build_oracle_summary_greedy(text, gold_summary, calc_score, lower, max_sentences):
        sentences = [" ".join(s).lower() if lower else " ".join(s) for s in text][:max_sentences]
        gold_summary = [" ".join(s).lower() if lower else " ".join(s) for s in gold_summary]

        output = {
            "text": text,
            "summary": gold_summary
        }

        def indices_to_text(indices):
            return " ".join([sentences[index] for index in sorted(list(indices))])

        n_sentences = len(sentences)
        scores = []
        final_score = -1.0
        final_indices = set()
        final_scores = []

        for _ in range(n_sentences):
            for i in range(n_sentences):
                if i in final_indices:
                    continue
                indices = copy.copy(final_indices)
                indices.add(i)
                summary = indices_to_text(indices)
                for sm in gold_summary:
                    try:
                        scores.append((calc_score(summary, sm), indices))
                    except:
                        pass

            best_score, best_indices = max(scores)
            scores = []
            if best_score <= final_score:
                break
            final_scores.append(best_score)
            final_score, final_indices = best_score, best_indices

        final_indices = list(final_indices)[:GREEDY_ORACLE_CONST]
        oracle_indices = [1 if i in final_indices else 0 for i in range(len(sentences))]
        output.update(
            {"sentences": sentences, "oracle": oracle_indices}
            )
        return output

    @staticmethod
    def calc_single_score(pred_summary, gold_summary, rouge):
        score = rouge.get_scores(pred_summary, gold_summary, avg=True)
        return (score['rouge-2']['f'] + score['rouge-1']['f'] + score['rouge-l']['f']) / 3

    def prepare_pytorch_data(self, data_tokenized, save_path_pattern):
        chunk = []
        chunk_id = 0

        for item in tqdm(data_tokenized):
            src = item['src']
            tgt = item['tgt']
            
            rouge = Rouge()
            calc_score = lambda x, y: self.calc_single_score(x, y, rouge)
            oracle_record = self.build_oracle_summary_greedy(src, tgt, calc_score=calc_score, lower=LOWER, max_sentences=MAX_SENTENCES)
            
            sent_labels = oracle_record['oracle']
            
            (src_indices,
                tgt_indices,
                    segments_ids,
                        cls_ids,
                            src_txt,
                                tgt_txt) = self.bert.preprocess(src, tgt)

            b_data_dict = {
                "src": src_indices, "tgt": tgt_indices,
                "segs": segments_ids, 'clss': cls_ids,
                'src_txt': src_txt, "tgt_txt": tgt_txt,
                'src_sent_labels': sent_labels
            }
            chunk.append(b_data_dict)
            if len(chunk) == CHUNK_SIZE:
                torch.save(chunk, save_path_pattern.format(chunk_id))
                chunk = []
                chunk_id += 1
                
        if chunk:
            torch.save(chunk, save_path_pattern.format(chunk_id))
import pickle

from src.config import config

PATH_TO_NAMES = config['preprocessing']['PATH_TO_NAMES']


class Utils:

    def __init__(self):
        with open(PATH_TO_NAMES, 'rb') as f:
            self.names = pickle.load(f)

    def check_fio(self, raw_text):
        splitted = raw_text.strip().split()

        if len(splitted) > 3:
            return False

        elif len(splitted) == 1:
            if splitted[0] in self.names:
                return True
            else:
                return False
    
        elif len(splitted) == 2:
            if splitted[0] in self.names and splitted[1] in self.names:
                return True
            else:
                return False
    
        elif len(splitted) == 3:
            if splitted[0] in self.names and splitted[1] in self.names and splitted[2] in self.names:
                return True
            else:
                return False
    
        else:
            return False

    @staticmethod
    def check_fio_in_text(row):
        splitted_fio = row.title.strip().split()

        if any([el in row.text for el in splitted_fio]):
            return True
        else:
            return False

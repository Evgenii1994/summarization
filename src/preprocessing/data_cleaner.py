import re

from src.preprocessing.constants import (
    HTML_REGEX,
    ADD_SPACE_REGEX,
    RUSSIAN_LETTERS_REGEX,
    NOT_TARGET_SYMBOLS_REGEX,
    REMOVE_SPACE_BETWEEN_PUNCTUATION,
    ADD_SPACE_BETWEEN_DIGITS_AND_LETTERS
)

class DataCleaner:

    @staticmethod
    def _clean_html(raw_text):
        cleaned_text = re.sub(HTML_REGEX, r'', raw_text)
        return cleaned_text
    
    @staticmethod
    def _clean_not_target_symbols(raw_text):
        cleaned_text = re.sub(NOT_TARGET_SYMBOLS_REGEX, r'', raw_text)
        return cleaned_text
    
    @staticmethod
    def _clean_double_whitespaces(raw_text):
        cleaned_text = r' '.join(raw_text.split())
        return cleaned_text
    
    @staticmethod
    def _check_russian_letters(raw_text):
        if re.search(RUSSIAN_LETTERS_REGEX, raw_text):
            return True
        else:
            return False
        
    @staticmethod
    def _add_space_after_some_punctuation(raw_text):
        cleaned_text = re.sub(ADD_SPACE_REGEX, r' ', raw_text)
        return cleaned_text
    
    @staticmethod
    def _add_space_between_digits_and_letters(raw_text):
        cleaned_text = re.sub(ADD_SPACE_BETWEEN_DIGITS_AND_LETTERS, r' \1 ', raw_text)
        return cleaned_text
    
    @staticmethod
    def _remove_space_between_punctuation(raw_text):
        cleaned_text = re.sub(REMOVE_SPACE_BETWEEN_PUNCTUATION, r'', raw_text)
        return cleaned_text

    def cleaning_pipeline(self, raw_text):
        if self._check_russian_letters(raw_text):
            cleaned_text = self._clean_html(raw_text)
            cleaned_text = self._clean_not_target_symbols(cleaned_text)
            cleaned_text = self._add_space_after_some_punctuation(cleaned_text)
            cleaned_text = self._add_space_between_digits_and_letters(cleaned_text)
            cleaned_text = self._clean_double_whitespaces(cleaned_text)
            cleaned_text = self._remove_space_between_punctuation(cleaned_text)
            return cleaned_text
        else:
            return ''
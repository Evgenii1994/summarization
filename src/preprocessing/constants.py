import re
import nltk

TOKENIZER = nltk.data.load('tokenizers/punkt/russian.pickle')

HTML_REGEX = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
NOT_TARGET_SYMBOLS_REGEX = re.compile(r'[^a-zA-Z0-9а-яА-Я \.,;:!\?\-"\')(]+')
RUSSIAN_LETTERS_REGEX = re.compile(r'[а-яА-Я]+')
ADD_SPACE_REGEX = re.compile(r'(?<=[.,?!:;])(?=[^\s])')
ADD_SPACE_BETWEEN_DIGITS_AND_LETTERS = re.compile('(\d+(\.\d+)?)')
REMOVE_SPACE_BETWEEN_PUNCTUATION = re.compile('(?<=[:.,!?()]) (?=[:.,!?()])')

SAVE_PATH_VAL_PATTERN = f'./data/pytorch_data/valid.{0}bert.pt'
SAVE_PATH_TEST_PATTERN = f'./data/pytorch_data/test.{0}bert.pt'
SAVE_PATH_TRAIN_PATTERN = f'./data/pytorch_data/train.{0}bert.pt'
SAVE_PATH_RAW_TEXT_PATTERN = f'./data/pytorch_data/raw_texts_pytorch_data/test.{0}bert.pt'

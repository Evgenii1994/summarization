from src.preprocessing.preprocessing import Preprocessing

if __name__ == '__main__':
    with open('./data/raw_texts.txt') as f:
        raw_texts = f.readlines()

    preprocessor = Preprocessing()
    preprocessor.data_preparation_pipeline_raw_text(raw_texts)
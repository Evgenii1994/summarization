## Задача суммаризации на данных РИА новости.

В репозитории представлен метод абстрактивной суммаризации, основанный на статье ["Text Summarization with Pretrained Encoders"](https://arxiv.org/abs/1908.08345).

Данный метод был выбран в виду исследования, направленного на изучение "направленной суммаризации" [GSum](https://arxiv.org/abs/2010.08014), которое показало как различные направляющие сигналы влияют на качество суммаризации в терминах метрики Rouge.

Использовался Python3.7.3

Структура репозитория:
<pre>
├─ data                                   # Данные
│   ├── models                            # Чекпоинты
│   ├── pytorch_data                      # Данные train/val/test
│   ├── rubert_cased_L-12_H-768_A-12_pt   # BERT
│   └── ...
│
├── notebooks                             # Юпитер тетрадки
│   └── Pipeline.ipynb                    # Пайплайн с комментариями
│   
├── results                               # Результаты валидации
│   ├── raw_texts_results                 # Результаты инференса
│   └── ...
│   
├── src                                   # Исходники
│   ├── evaluation                        # Расчет метрик качества
│   │   └── ...             
│   ├── preprocessing                     # Препроцессинг данных
│   │   └── ...              
│   ├── PreSumm                           # https://github.com/nlpyang/PreSumm/tree/master
│   │   └── ...
│   └── ...
│
├── config.toml                           # Общий конфигурационный файл
├── run_evaluation.py                     # Рассчитать метрики качества
├── run_preprocessing.py                  # Подготовить данные
├── run_training.py                       # Запустить обучение
├── run_validation.py                     # Выбор лучшей модели среди чекпоинтов
├── run_preprocessing_raw_texts.py        # Подготовка данных для инференса
├── run_evaluation_raw_texts.py           # Инференс
└── ...
</pre>

### В общем и целом процесс таков:
1. Установить зависимости `pip install -r requirements.txt`, если возникает ошибка - `pip install --upgrade pip`
1. Скачать данные [РИА новости](https://github.com/RossiyaSegodnya/ria_news_dataset) и положить в `./data/`
2. Скачать [RuBert](http://files.deeppavlov.ai/deeppavlov_data/bert/rubert_cased_L-12_H-768_A-12_pt.tar.gz) и положить в `./data/`
3. Подготавливаем данные в формате для обучения, `python run_preprocessing.py`, параметры препроцессинга в `config[preprocessing]`
4. Запускаем обучение, `python run_training.py`, параметры обучения в `config[train]`
5. Выбираем лучшую модель, `python run_validation.py`, параметры валидации в `config[validate]`, в логах модели сортируются по лоссу
6. Рассчитываем качество на тесте, `python run_evaluation.py`, параметры в `config[evaluate]`, нужно указать пути *

*В результате валидации в `./results/` генерятся файлы `*.candidate, *.gold, *.raw_src` в них можно посмотреть на предсказания модели, истинные заголовки и сами тексты соответственно.

*В более удобном формате, с результатом постпроцессинга, это можно сделать в `./notebooks/Pipeline.ipynb`

### Инференс:
1. Скачать [чекпоинт](https://drive.google.com/drive/folders/1CsQjbJfBVD_G-yHqvUSTrGBUfd4ZhpAS?usp=sharing) и положить в `./data/models/`
2. Убедиться, что в `config[evaluate_raw][TEST_FROM]` указан путь до нужного чекпоинта
3. Заполнить файл `./data/raw_texts.txt`, где в каждой строке лежит текст
4. Подготовить данные в формате для инференса `python run_preprocessing_raw_texts.py`, параметры в `config[preprocessing]`
5. Запустить инференс `python run_evaluation_raw_texts.py`, параметры в `config[evaluate_raw]`

Результаты инференса будут лежать в `./results/raw_texts_results`, сгенерированные заголовки сортируются (не успел исправить), поэтому лучше тестить на небольшом количестве данных
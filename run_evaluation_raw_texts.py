import subprocess
import shlex
import os

from src.config import config

PATH_TO_BERT = config['preprocessing']['PATH_TO_BERT']
TASK = config['evaluate_raw']['TASK']
MODE = config['evaluate_raw']['MODE']
TEST_FROM = config['evaluate_raw']['TEST_FROM']
BATCH_SIZE = config['evaluate_raw']['BATCH_SIZE']
TEST_BATCH_SIZE = config['evaluate_raw']['TEST_BATCH_SIZE']
BERT_DATA_PATH = config['evaluate_raw']['BERT_DATA_PATH']
LOG_FILE = config['evaluate_raw']['LOG_FILE']
MODEL_PATH = config['evaluate_raw']['MODEL_PATH']
SEP_OPTIM = config['evaluate_raw']['SEP_OPTIM']
USE_INTERVAL = config['evaluate_raw']['USE_INTERVAL']
VISIBLE_GPUS = config['evaluate_raw']['VISIBLE_GPUS']
MAX_POS = config['evaluate_raw']['MAX_POS']
MAX_LENGTH = config['evaluate_raw']['MAX_LENGTH']
ALPHA = config['evaluate_raw']['ALPHA']
MIN_LENGTH = config['evaluate_raw']['MIN_LENGTH']
RESULT_PATH = config['evaluate_raw']['RESULT_PATH']

os.environ ['BERT_PATH'] = PATH_TO_BERT


if __name__ == '__main__':
    subprocess.call(
        shlex.split(
            f'python ./src/PreSumm/src/train.py\
            -task {TASK}\
            -mode {MODE}\
            -test_from {TEST_FROM}\
            -batch_size {BATCH_SIZE}\
            -test_batch_size {TEST_BATCH_SIZE}\
            -bert_data_path {BERT_DATA_PATH}\
            -log_file {LOG_FILE}\
            -model_path {MODEL_PATH}\
            -sep_optim {SEP_OPTIM}\
            -use_interval {USE_INTERVAL}\
            -visible_gpus {VISIBLE_GPUS}\
            -max_pos {MAX_POS}\
            -max_length {MAX_LENGTH}\
            -alpha {ALPHA}\
            -min_length {MIN_LENGTH}\
            -result_path {RESULT_PATH}'
            )
            )